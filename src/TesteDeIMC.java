import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TesteDeIMC {
	
	Paciente paciente;
	
	
	@Test
	public void baixoPesoMuitoGrave() {
		// = IMC abaixo de 16 kg/m�
		paciente = new Paciente(50, 1.80);
		assertEquals(15.43, paciente.calcularIMC(), 0.01);
	}
	
	@Test
	public void baixoPesoGrave() {
		//IMC entre 16 e 16,99 kg/m�
		paciente = new Paciente(60, 1.90);
		assertEquals(16.62, paciente.calcularIMC(), 0.001);
	}
	
	@Test
	public void baixoPeso() {
		//= IMC entre 17 e 18,49 kg/m�
		paciente = new Paciente(70, 1.95);
		assertEquals(18.41, paciente.calcularIMC(), 0.01);
		
	}
	
	@Test
	public void pesoNormal() {
		//= IMC entre 18,50 e 24,99 kg/m�
		paciente = new Paciente(70, 1.70);
		assertEquals(24.22, paciente.calcularIMC(), 0.01);
	}
	
	@Test
	public void sobrepeso() {
		//= IMC entre 25 e 29,99 kg/m�
		paciente = new Paciente(100, 2);
		assertEquals(25.00, paciente.calcularIMC(), 0.01);
	}
	
	@Test
	public void obesidadeGrauUm() {
		//= IMC entre 30 e 34,99 kg/m�
		paciente = new Paciente(95, 1.65);
		assertEquals(34.89, paciente.calcularIMC(), 0.01);
	}
	
	@Test
	public void obesidadeGrauDois() {
		//= IMC entre 35 e 39,99 kg/m�
		paciente = new Paciente(115, 1.70);
		assertEquals(39.79, paciente.calcularIMC(), 0.01);
	}
	
	@Test
	public void obesidadeGrauTres() {
		//= (obesidade m�rbida) = IMC maior que 40 kg/m�
		paciente = new Paciente(140, 1.50);
		assertEquals(62.22, paciente.calcularIMC(), 0.01);
	}
			
}
