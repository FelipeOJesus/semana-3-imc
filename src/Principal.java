public class Principal {

	public static void main(String[] args) {
		Paciente p1 = new Paciente(70.6, 1.71);
		Paciente p2 = new Paciente(103.2, 1.68);
		Paciente p3 = new Paciente(80.7, 2.12);

		System.out.println(p1.calcularIMC());
		System.out.println(p1.diagnostico());
		System.out.println(p2.calcularIMC());
		System.out.println(p2.diagnostico());
		System.out.println(p3.calcularIMC());
		System.out.println(p3.diagnostico());

	}

}
